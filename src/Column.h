#ifndef COLUMN_H
#define COLUMN_H

#include <QString>

namespace QSpreadsheetFormat {

    /** A column of a table. */
    class Column {

    public:
        /**
         * Returns the width of the column.
         * @return width of the column, in points. If negative, default
         *         column width is used.
         */
        double getWidth() const;

        /**
         * Sets the width of the column.
         * @param width new width of the column, in points. If negative,
         *        default column width is used.
         */
        void setWidth(double width);

        /** Empty destructor */
        ~Column() {}
    
    private:

        /** Stores the column width */
        double width;

    };

}

#endif
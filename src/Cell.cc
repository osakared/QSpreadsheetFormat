#include "Cell.h"

#include <QRegExp>

using namespace QSpreadsheetFormat;

Cell::Cell()
{
    type = NONE;
    hAlignment = HADEFAULT;
    vAlignment = VADEFAULT;
}

Cell::~Cell()
{
}

QString Cell::getText() const
{
    return value.toString();
}

void Cell::setText(const QString &t)
{
    type = TEXT;
    value.setValue(t);
}

qlonglong Cell::getLong() const
{
    return value.toLongLong();
}

void Cell::setLong(qlonglong l)
{
    type = LONG;
    value.setValue(l);
}

double Cell::getDouble() const
{
    return value.toDouble();
}

void Cell::setDouble(double d)
{
    type = DOUBLE;
    value.setValue(d);
}

QDate Cell::getDate() const
{
    return value.toDate();
}

void Cell::setDate(const QDate &d)
{
    type = DATE;
    value.setValue(d);
}

QTime Cell::getTime() const
{
    return value.toTime();
}

void Cell::setTime(const QTime &t)
{
    type = TIME;
    value.setValue(t);
}

QString Cell::getFormula() const {
    return getText();
}

void Cell::setFormula(const QString &f) {
    QRegExp r("sum\(\d+:\d+\)");
    r.setCaseSensitivity(Qt::CaseInsensitive);
    if (r.exactMatch(f)) {
        type = FORMULA;
        setText(f);
    }
    else {
        clear();
    }
}

void Cell::clear() {
    type = NONE;
    value.clear();
}

Cell::Type Cell::getType() const {
    return type;
}

Cell::HAlignment Cell::getHAlignment() const {
    return hAlignment;
}

void Cell::setHAlignment(HAlignment h) {
    hAlignment = h;
}

Cell::VAlignment Cell::getVAlignment() const {
    return vAlignment;
}

void Cell::setVAlignment(VAlignment v) {
    vAlignment = v;
}

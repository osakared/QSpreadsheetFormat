// platform-specific includes and declarations
// #ifdef WIN32
//     // this symbol should not be defined on the Win32 project that uses
//     // splib.dll; this way the project will see SPLIB_API functions as being
//     // imported from a DLL
//     #ifdef QSPREADSHEET_LIB_API
//         #define QSPREADSHEET_LIB_API __declspec(dllexport)
//     #else
//         #define QSPREADSHEET_LIB_API __declspec(dllimport)
//     #endif // QSPREADSHEET_LIB_API
// #else
//     #define QSPREADSHEET_LIB_API
// #endif // WIN32

#include "Spreadsheet.h"
#include "Table.h"
#include "Cell.h"
#include "Row.h"
#include "Column.h"
#include "Columns.h"
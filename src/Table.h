#ifndef TABLE_H
#define TABLE_H

#include <QString>

// forward declarations
class Cell;
class Rows;
class Columns;

namespace QSpreadsheetFormat {

    /**
     * A spreadsheet table. This interface provides methods for accessing
     * table cells as well as other table properties.
     */
    class Table {

        public:
            /**
             * Retrieves a cell at the specified position.
             * If the cell at the specified position is empty (that is, no cell
             * object exists for it), the corresponding cell object is created
             * and initialized to default, and a reference to that object is
             * returned. If the cell at the specified position is not empty
             * (that is, a cell object exists for it), a reference to that object
             * is returned.
             * @param column column index for which to retrieve the cell
             * @param row row index for which to retrieve the cell
             * @return a reference to the cell object
             * @throw IllegalArgumentException if <code>column</code> or
             *        <code>row</code> is invalid
             */
            virtual Cell &cell(int column, int row) = 0;

            /**
             * Retrieves a cell at the specified position.
             * The position is specified in the
             * &lt;column&nbsp;letter(s)&gt;&lt;row&nbsp;number&gt; format used
             * by most spreadsheet applications to identify cells.
             * If the cell at the specified position is empty (that is, no cell
             * object exists for it), the corresponding cell object is created
             * and initialized to default, and a reference to that object is
             * returned. If the cell at the specified position is not empty
             * (that is, a cell object exists for it), a reference to that object
             * is returned.
             * @param columnrow a string in the
             *        &lt;column&nbsp;letter(s)&gt;&lt;row&nbsp;number&gt;
             *        format; for example, <code>"A1"</code>, <code>"D20"</code>,
             *        <code>"AA9"</code>
             * @return a reference to the cell object
             * @throw IllegalArgumentException if <code>columnrow</code> is invalid
             */
            virtual Cell &cell(const QString &columnrow) = 0;

            /**
             * Clears the cell at the specified position. Deletes the cell
             * object associated with the position, if it exists.
             * @param column column index
             * @param row row index
             * @throw IllegalArgumentException if <code>column</code> or
             *        <code>row</code> is invalid
             */
            virtual void clearCell(int column, int row) = 0;

            /**
             * Clears the cell at the specified position. Deletes the cell
             * object associated with the position, if it exists.
             * @param columnrow a string in the
             *        &lt;column&nbsp;letter(s)&gt;&lt;row&nbsp;number&gt; format
             * @throw IllegalArgumentException if <code>columnrow</code> is invalid
             */
            virtual void clearCell(const QString &columnrow) = 0;

            /**
             * Clears a rectangular range of cells. The range is identified by
             * the positions of the two corners of the rectangle. The rectangle
             * does not have to be normalized.
             * @param column1 column index of the top left corner of the range
             * @param row1 row index of the top left corner of the range
             * @param column2 column index of the bottom right corner of the range
             * @param row2 row index of the bottom right corner of the range
             * @throw IllegalArgumentException if <code>column1</code>,
             *        <code>row1</code>, <code>column2</code>, or
             *        <code>row2</code> is invalid
             */
            virtual void clearRange(int column1, int row1,
                                    int column2, int row2) = 0;

            /**
             * Clears a rectangular range of cells. The range is identified by
             * the positions of the two corners of the rectangle. The rectangle
             * does not have to be normalized.
             * @param columnrow1 a string in the
             *        &lt;column&nbsp;letter(s)&gt;&lt;row&nbsp;number&gt; format
             *        that identifies the top left corner of the range
             * @param columnrow2 a string in the
             *        &lt;column&nbsp;letter(s)&gt;&lt;row&nbsp;number&gt; format
             *        that identifies the bottom right corner of the range
             * @throw IllegalArgumentException if <code>columnrow1</code> or
             *        <code>columnrow2</code> is invalid
             */
            virtual void clearRange(const QString &columnrow1,
                                    const QString &columnrow2) = 0;

            /**
             * Determines if the specified position is empty.
             * @param column column index
             * @param row row index
             * @return true if the specified position is empty (that is, no
             *         cell object exists at it); false otherwise
             * @throw IllegalArgumentException if <code>column</code> or
             *        <code>row</code> is invalid
             */
            virtual bool isEmptyCell(int column, int row) const = 0;

            /**
             * Determines if the specified position is empty.
             * @param columnrow a string in the
             *        &lt;column&nbsp;letter(s)&gt;&lt;row&nbsp;number&gt; format
             * @return true if the specified position is empty (that is, no
             *         cell object exists at it); false otherwise
             * @throw IllegalArgumentException if <code>columnrow</code> is
             *        invalid
             */
            virtual bool isEmptyCell(const QString &columnrow) const = 0;

            /**
             * Determines the first (leftmost) table column containing
             * non-empty cells.
             * @return the index of the leftmost table column containing
             *         non-empty cells. -1 if there are no cells in the table.
             */
            virtual int firstColumn() const = 0;

            /**
             * Determines the first (topmost) table row containing
             * non-empty cells.
             * @return the index of the topmost table row containing
             *         non-empty cells. -1 if there are no cells in the table.
             */
            virtual int firstRow() const = 0;

            /**
             * Determines the last (rightmost) table column containing
             * non-empty cells.
             * @return the index of the rightmost table column containing
             *         non-empty cells. -1 if there are no cells in the table.
             */
            virtual int lastColumn() const = 0;

            /**
             * Determines the last (bottommost) table row containing
             * non-empty cells.
             * @return the index of the bottommost table row containing
             *         non-empty cells. -1 if there are no cells in the table.
             */
            virtual int lastRow() const = 0;

            /**
             * Retrieves the collection of rows.
             * @return collection of rows.
             */
            virtual Rows& rows() = 0;

            /**
             * Retrieves the collection of columns.
             * @return collection of columns.
             */
            virtual Columns& columns() = 0;

            /**
             * Retrieves the name of the table.
             * @return a pointer to the string containing the name of the table
             */
            virtual const QString &getName() const = 0;

            /**
             * Sets the name of the table.
             * @param name a pointer to the string specifying the new name
             *        of the table
             * @throw IllegalArgumentException if a table with this name
             *        already exists in the spreadsheet
             */
            virtual void setName(const QString &name) = 0;

            /** Empty virtual destructor */
            virtual ~Table() {}
    };

}

#endif
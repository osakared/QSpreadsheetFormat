#ifndef COLUMNS_H
#define COLUMNS_H

#include "Column.h"

namespace QSpreadsheetFormat {

    /**
     * A collection of columns. The semantics of the collection is defined by
     * the <code>QList</code> interface, plus this interface adds
     * the ability to retrieve a column by column string.
     */
    class Columns : public QList<Column> {

    public:

        /**
         * Retrieves a column by index. If the column with the given
         * index does not exist, it is created.
         * @param index index of the column
         * @return reference to the column
         */
        virtual Column &get(int index) = 0;

        /**
         * Retrieves a column by column character (or character string).
         * If the specified column does not exist, it is created.
         * @param column string that identifies the column, for example, "AA"
         * @return reference to the column
         */
        virtual Column &get(const QString &column) = 0;

    };

}

#endif
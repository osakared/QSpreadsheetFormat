#ifndef CELL_H
#define CELL_H

#include <QList>
#include <QDate>
#include <QString>
#include <QVariant>

namespace QSpreadsheetFormat {

    /**
     * A spreadsheet cell. This interface defines methods to obtain and modify
     * cell properties such as cell content, type, and formatting.
     */
    class Cell {

    public:
    
        /**
         * Retrieves the cell text.
         * The method succeeds if the current cell type is
         * <code>TEXT</code>, otherwise invalid text is returned.
         * @return a string containing the cell text
         */
        QString getText() const;

        /**
         * Sets the cell type to <code>TEXT</code> and updates the cell
         * text.
         * @param text a reference to the string specifying the new cell text;
         */
        void setText(const QString &text);

        /**
         * Retrieves the long integer value of the cell.
         * The method succeeds if the current cell type is
         * <code>LONG</code>, otherwise returns 0.
         * @return the value of the cell
         */
        qlonglong getLong() const;

        /**
         * Sets the cell type to <code>LONG</code> and updates the value
         * of the cell.
         * @param l new long integer value of the cell
         */
        void setLong(qlonglong l);

        /**
         * Retrieves the double value of the cell.
         * The method succeeds if the current cell type is
         * <code>DOUBLE</code>, otherwise an invalid value is returned.
         * @return the value of the cell
         */
        double getDouble() const;

        /**
         * Sets the cell type to <code>DOUBLE</code> and updates the value
         * of the cell.
         * @param d new value of the cell
         */
        void setDouble(double d);

        /**
         * Retrieves the date stored in the cell.
         * The method succeeds if the current cell type is
         * <code>DATE</code>, otherwise an invalid date is returned.
         * @return the date stored in the cell
         */
        QDate getDate() const;

        /**
         * Sets the cell type to <code>DATE</code> and updates the date
         * stored in the cell.
         * @param date the new date to store in the cell
         */
        void setDate(const QDate &date);

        /**
         * Retrieves the time stored in the cell.
         * The method succeeds if the current cell type is
         * <code>TIME</code>, otherwise an invalid time is returned.
         * @return the time stored in the cell
         */
        QTime getTime() const;

        /**
         * Sets the cell type to <code>TIME</code> and updates the time
         * stored in the cell.
         * @param time the new time to store in the cell
         */
        void setTime(const QTime &time);

        /**
         * Retrieves the formula stored in the cell.
         * The method succeeds if the current cell type is
         * <code>FORMULA</code>, otherwise an invalid formula is returned.
         * @return the formula stored in the cell
         */
        QString getFormula() const;

        /**
         * Sets the cell type to <code>FORMULA</code> and updates the
         * formula stored in the cell.
         * Only sums of cell ranges are supported, such as "SUM(A1:B10)".
         * An invalid formula will result in an empty cell.
         * @param formula the new formula to store in the cell
         */
        void setFormula(const QString &formula);

        /**
         * Sets the cell type to <code>NONE</code> and clears all data
         * stored in the cell.
         */
        void clear();

        /** Possible cell types. */
        enum Type {
            /** No type (empty cell) */
            NONE, 
            /** Text */
            TEXT, 
            /** Long integer */
            LONG,
            /** Double precision floating point number  */
            DOUBLE,
            /** Date */
            DATE, 
            /** Time */
            TIME,
            /** Formula */
            FORMULA
        };

        /**
         * Retrieves the cell type.
         * @return the cell type
         */
        Type getType() const;

        /**
         * The enumeration lists all possible horizontal alignment
         * possibilities.
         */
        enum HAlignment {
            /** Default alignment */
            HADEFAULT,
            /** Left alignment */
            LEFT,
            /** Center alignment */
            CENTER,
            /** Right alignment */
            RIGHT,
            /** Justified alignment */
            JUSTIFIED,
            /** Filled alignment */
            FILLED
        };

        /**
         * Retrieves the horizontal alignment of the cell.
         * @return horizontal alignment
         */
        HAlignment getHAlignment() const;

        /**
         * Sets the horizontal alignment of the cell.
         * @param hAlignment horizontal alignment
         */
        void setHAlignment(HAlignment hAlignment);

        /**
         * The enumeration lists all possible vertical alignment
         * possibilities.
         */
        enum VAlignment {
            /** Default alignment */
            VADEFAULT, 
            /** Top alignment */
            TOP,
            /** Middle alignment */
            MIDDLE,
            /** Bottom alignment */
            BOTTOM
        };

        /**
         * Retrieves the vertical alignment of the cell.
         * @return vertical alignment
         */
        VAlignment getVAlignment() const;

        /**
         * Sets the vertical alignment of the cell.
         * @param vAlignment vertical alignment
         */
        void setVAlignment(VAlignment vAlignment);

        /** Empty destructor */
        ~Cell() {}
    
    private:

        /** The cell type */
        Type type;

        /** The cell text */
        QVariant value;

        /** The horizontal alignment of the cell */
        HAlignment hAlignment;

        /** The vertical alignment of the cell */
        VAlignment vAlignment;

    };

    typedef QList<Cell> Cells;

}

#endif
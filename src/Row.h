#ifndef ROW_H
#define ROW_H

#include "Cell.h"

namespace QSpreadsheetFormat {

    /** A row of a table. */
    class Row {

        public:
            /**
             * Returns the height of the row.
             * @return height of the row, in points. If negative,
             *         default row height is used.
             */
            virtual double getHeight() const = 0;

            /**
             * Sets the height of the row.
             * @param height new height of the row, in points. If negative,
             *        default row height is used.
             */
            virtual void setHeight(double height) = 0;

            /**
             * Returns the collection of cells of this row.
             * @return the collection of cells of this row
             */
            virtual Cells &cells() = 0;

            /** Empty virtual destructor */
            virtual ~Row() {}
    };

    typedef QList<Row> Rows;

}

#endif
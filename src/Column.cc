#include "Column.h"

using namespace QSpreadsheetFormat;

Column::Column()
{
    width = -1;
}

double Column::getWidth() const
{
    return width;
}

void Column::setWidth(double w)
{
    width = w;
}

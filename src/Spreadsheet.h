#ifndef SPREADSHEET_H
#define SPREADSHEET_H

#include <QString>

namespace QSpreadsheetFormat {

    // forward declarations
    class Table;

    /**
     * A spreadsheet object. The spreadsheet consists of a number of tables.
     * This interface provides methods for adding, removing, and accessing
     * the tables in the spreadsheet.
     */
    class Spreadsheet {
        
        public:
            /**
             * Creates a new table and inserts it into the spreadsheet.
             * @param index the index at which to insert the table
             * @param name a pointer to the name of the table
             * @return a reference to the newly created table
             * @throw IllegalArgumentException if <code>index</code> is negative
             *        or greater than the value returned by tableCount();
             *        or if <code>name</code> is 0;
             *        or if a table with the specified name already exists
             */
            virtual Table &insertTable(int index, const QString &name) = 0;

            /**
             * Retrieves a table by index.
             * @param index the index at which to retrieve the table
             * @return a reference to the table object
             * @throw IllegalArgumentException if <code>index</code> is
             *        negative or greater than or equal to the value returned by
             *        tableCount()
             */
            virtual Table &table(int index) = 0;

            /**
             * Retrieves a table by name.
             * @param name the name of the table to retrieve
             * @return a reference to the table object
             * @throw IllegalArgumentException if <code>name</code> is 0
             *        or if a table with the specified name does not exist
             *        in the spreadsheet
             */
            virtual Table &table(const QString &name) = 0;

            /**
             * Retrieves the number of tables in the spreadsheet.
             * @return the number of tables in the spreadsheet
             */
            virtual int tableCount() const = 0;

            /**
             * Determines if a table with the specified name already exists in
             * the spreadsheet.
             * @param name a pointer to a string that specifies the table name
             * @return true if a table with the specified name exists;
             *         false otherwise
             * @throw IllegalArgumentException if <code>name</code> is 0
             */
            virtual bool tableExists(const QString &name) = 0;

            /**
             * Removes a table from the spreadsheet.
             * @param index the index at which to remove the table
             * @throw IllegalArgumentException if <code>index</code> is
             *        negative or greater than or equal to the value returned by
             *        tableCount()
             */
            virtual void removeTable(int index) = 0;

            /**
             * Removes a table from the spreadsheet.
             * @param name the name of the table to remove
             * @throw IllegalArgumentException if <code>name</code> is 0
             *        or if a table with the specified name does not exist
             *        in the spreadsheet
             */
            virtual void removeTable(const QString &name) = 0;

            /** Empty virtual destructor */
            virtual ~Spreadsheet() {}
    };

}

#endif
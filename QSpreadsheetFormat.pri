INCLUDEPATH += $$PWD/src
DEPENDPATH += $$PWD/src

HEADERS += \
    $$PWD/Cell.h
    $$PWD/Column.h
    $$PWD/Columns.h
    $$PWD/QSpreadsheetFormat.h
    $$PWD/Row.h
    $$PWD/Spreadsheet.h
    $$PWD/Table.h

SOURCES += \
    $$PWD/Cell.cc
    $$PWD/Column.cc
